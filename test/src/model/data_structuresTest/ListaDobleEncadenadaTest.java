package model.data_structuresTest;

import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;

public class ListaDobleEncadenadaTest extends TestCase{

	
	protected ListaDobleEncadenada<Object> lista;
	
	public void setUp(){
		lista = new ListaDobleEncadenada<Object>();
	}
	
	public void addTest()
	{
		lista.agregarElementoFinal("A");
		assertTrue(lista.darElemento(0).equals("A"));
		lista.agregarElementoFinal("B");
		assertTrue(lista.darElemento(1).equals("B"));
	}
	
	public void darElementoTest(){
		lista.agregarElementoFinal("A");
		lista.agregarElementoFinal("B");
		assertTrue(lista.darElemento(0).equals("A"));
		assertTrue(lista.darElemento(1).equals("B"));
	}
	
	public void darNumeroElementosTest(){
		lista.agregarElementoFinal("A");
		lista.agregarElementoFinal("B");
		lista.agregarElementoFinal("C");
		assertTrue(lista.darNumeroElementos() == 3);
	}
	
	public void darElementoPosicioAnctualTest(){
		lista.agregarElementoFinal("A");
		lista.agregarElementoFinal("B");
		lista.agregarElementoFinal("C");
		assertTrue(lista.darElementoPosicionActual().equals("A"));
	}
	
	public void avanzarSiguientePosicionTest(){
		lista.agregarElementoFinal("A");
		lista.agregarElementoFinal("B");
		lista.agregarElementoFinal("C");
		assertTrue(lista.darElementoPosicionActual().equals("A"));
		lista.avanzarSiguientePosicion();
		assertTrue(lista.darElementoPosicionActual().equals("B"));
	}
	
public void retrocederPosicionAnteriorTest(){
	lista.agregarElementoFinal("A");
	lista.agregarElementoFinal("B");
	lista.agregarElementoFinal("C");
	assertTrue(lista.darElementoPosicionActual().equals("A"));
	lista.avanzarSiguientePosicion();
	assertTrue(lista.darElementoPosicionActual().equals("B"));
	lista.retrocederPosicionAnterior();
	assertTrue(lista.darElementoPosicionActual().equals("A"));
	}
	
	
}
